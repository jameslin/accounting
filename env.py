import os
import configparser
from os.path import dirname

try:
    BASE_DIR = dirname(__file__)
    config = configparser.ConfigParser()
    config.optionxform = str
    if os.path.exists(os.path.join(BASE_DIR, 'environment.ini')):
        config.read(os.path.join(BASE_DIR, 'environment.ini'))
    else:
        config.read(os.path.join(BASE_DIR, 'environment-dev.ini'))
    for name, value in config['ENVIRONMENTS'].items():
        os.environ.setdefault(name, value)
except:
    pass

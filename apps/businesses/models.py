# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse
from apps.contribs.models import BaseModel


class Business(BaseModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    name = models.CharField(max_length=255)
    gst_number = models.CharField(max_length=255, blank=True)
    logo = models.ImageField(upload_to='businesses/logos', blank=True)
    slogan = models.CharField(max_length=255, blank=True)
    address1 = models.CharField(max_length=255)
    address2 = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255)
    post_code = models.CharField(max_length=255, blank=True)
    phone = models.CharField(max_length=255, blank=True)
    fax = models.CharField(max_length=255, blank=True)
    email = models.EmailField()
    invoice_header_content = models.TextField(blank=True)
    invoice_footer_content = models.TextField(blank=True)
    quote_header_content = models.TextField(blank=True)
    quote_footer_content = models.TextField(blank=True)
    initial_invoice_number = models.IntegerField(default=10000)
    initial_quote_number = models.IntegerField(default=10000)
    default_tax = models.DecimalField(max_digits=5, decimal_places=4, default='0.15')
    default_quote_expiry_days = models.IntegerField(default=14)
    default_invoice_due_days = models.IntegerField(default=30)
    invoice_theme = models.ForeignKey('invoices.Theme', default=1)
    quote_theme = models.ForeignKey('quotes.Theme', default=1)

    def get_absolute_url(self):
        return reverse('businesses:update', args=[self.uuid])

    @property
    def last_invoice(self):
        return self.invoices.order_by('-id').first()

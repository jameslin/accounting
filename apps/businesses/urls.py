# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.conf.urls import url
from . import views
from apps.contribs.regex.url import UUID4_REGEX_PARAM

urlpatterns = [
    url(r'^{}$'.format(UUID4_REGEX_PARAM), views.UpdateBusinessView.as_view(), name="update"),
    url(r'^create$', views.CreateBusinessView.as_view(), name="create"),
    url(r'^(?P<page>\d+)$', views.ListBusinessView.as_view(), name="list"),
    url(r'^$', views.ListBusinessView.as_view(), name="list"),
]

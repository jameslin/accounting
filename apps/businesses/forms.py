# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from .models import Business
from apps.contribs.forms.widgets import PercentageInput
from apps.contribs.forms.fields import PercentageField
import floppyforms.__future__ as forms


class BusinessForm(forms.ModelForm):
    default_tax = PercentageField(widget=PercentageInput(attrs={'step': 'any'}), label="Default Tax %")

    class Meta:
        model = Business
        exclude = ['uuid', 'user']

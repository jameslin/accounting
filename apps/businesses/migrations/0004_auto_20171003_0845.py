# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2017-10-02 19:45
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('businesses', '0003_auto_20171003_0821'),
    ]

    operations = [
        migrations.RenameField(
            model_name='business',
            old_name='theme',
            new_name='invoice_theme',
        ),
    ]

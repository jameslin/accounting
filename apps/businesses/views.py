# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.views.generic import CreateView, UpdateView, ListView
from apps.contribs.views.mixins import NormalSecurityMixin, AutoUserMixin, OwnerQuerySetMixin
from .models import Business
from .forms import BusinessForm


class CreateBusinessView(NormalSecurityMixin, AutoUserMixin, CreateView):
    model = Business
    form_class = BusinessForm
    template_name = 'businesses/create.html'


class UpdateBusinessView(NormalSecurityMixin, UpdateView):
    model = Business
    form_class = BusinessForm
    template_name = 'businesses/update.html'
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'


class ListBusinessView(NormalSecurityMixin, OwnerQuerySetMixin, ListView):
    model = Business
    template_name = 'businesses/list.html'

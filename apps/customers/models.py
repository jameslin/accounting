# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse
from apps.contribs.models import BaseModel


class Customer(BaseModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    name = models.CharField(max_length=255)
    address1 = models.CharField(max_length=255)
    address2 = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255, blank=True)
    post_code = models.CharField(max_length=255, blank=True)
    email = models.EmailField(blank=True)

    def get_absolute_url(self):
        return reverse('customers:update', args=[self.uuid])

    @property
    def last_invoice(self):
        return self.invoices.order_by('-id').first()

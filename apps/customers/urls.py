# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.conf.urls import url
from . import views
from apps.contribs.regex.url import UUID4_REGEX_PARAM

urlpatterns = [
    url(r'^{}$'.format(UUID4_REGEX_PARAM), views.UpdateCustomerView.as_view(), name="update"),
    url(r'^create$', views.CreateCustomerView.as_view(), name="create"),
    url(r'^(?P<page>\d+)$', views.ListCustomerView.as_view(), name="list"),
    url(r'^$', views.ListCustomerView.as_view(), name="list"),
]

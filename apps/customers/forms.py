# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from .models import Customer
import floppyforms.__future__ as forms


class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customer
        exclude = ['uuid', 'user']

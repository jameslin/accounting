# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.views.generic import CreateView, UpdateView, ListView
from apps.contribs.views.mixins import NormalSecurityMixin, AutoUserMixin, OwnerQuerySetMixin
from .models import Customer
from .forms import CustomerForm


class CreateCustomerView(NormalSecurityMixin, AutoUserMixin, CreateView):
    model = Customer
    form_class = CustomerForm
    template_name = 'customers/create.html'


class UpdateCustomerView(NormalSecurityMixin, UpdateView):
    model = Customer
    form_class = CustomerForm
    template_name = 'customers/update.html'
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'


class ListCustomerView(NormalSecurityMixin, OwnerQuerySetMixin, ListView):
    model = Customer
    template_name = 'customers/list.html'

from django.contrib import admin
from .models import Invoice, Theme


admin.site.register(Invoice)
admin.site.register(Theme)

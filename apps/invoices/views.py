# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
import logging
from django.shortcuts import get_object_or_404
from django.views.generic import CreateView, ListView, DetailView
from django.template import Context
from django.template.loader import get_template
from django.contrib import messages
from apps.contribs.views.mixins import NormalSecurityMixin, AutoUserMixin, OwnerQuerySetMixin, InjectRequestFormMixin, TokenSecurityMixin
from extra_views import UpdateWithInlinesView, InlineFormSet
from wkhtmltopdf.views import PDFTemplateView
from .models import Invoice, LineItem
from .forms import LineItemForm, InvoiceCreateForm, InvoiceUpdateForm

log = logging.getLogger(__name__)


class ListInvoiceView(NormalSecurityMixin, OwnerQuerySetMixin, ListView):
    model = Invoice
    template_name = 'invoices/list.html'
    user_field_name = 'business__user'

    def get_context_data(self, **kwargs):
        ctx = super(ListInvoiceView, self).get_context_data(**kwargs)
        ctx['overdue'] = Invoice.objects.filter(business__user=self.request.user).overdue()
        return ctx


class ListOverdueInvoiceView(NormalSecurityMixin, OwnerQuerySetMixin, ListView):
    model = Invoice
    template_name = 'invoices/overdue.html'
    queryset = Invoice.objects.overdue()
    user_field_name = 'business__user'


class CreateInvoiceView(NormalSecurityMixin, AutoUserMixin, InjectRequestFormMixin, CreateView):
    model = Invoice
    form_class = InvoiceCreateForm
    template_name = 'invoices/create.html'

    def get_initial(self):
        data = super(CreateInvoiceView, self).get_initial()
        # if url contains preselected options, then use them
        # eg. create invoice from business, customer list
        if self.request.GET.get('business'):
            data['business'] = self.request.GET.get('business')
        if self.request.GET.get('customer'):
            data['customer'] = self.request.GET.get('customer')
        return data


class LineItemInline(NormalSecurityMixin, InlineFormSet):
    model = LineItem
    form_class = LineItemForm
    extra = 10
    max_num = 20


class UpdateInvoiceView(NormalSecurityMixin, InjectRequestFormMixin, UpdateWithInlinesView):
    model = Invoice
    form_class = InvoiceUpdateForm
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'
    inlines = [LineItemInline]
    template_name = 'invoices/update.html'


class InvoicePDFEmbededView(NormalSecurityMixin, DetailView):
    template_name = 'invoices/pdf.html'
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'
    model = Invoice

    def get_context_data(self, **kwargs):
        ctx = super(InvoicePDFEmbededView, self).get_context_data(**kwargs)
        ctx['email_body'] = self.request.POST.get('email_body') or get_template('invoices/email/body.txt').render(Context({'object': self.object}))
        return ctx

    def post(self, request, *args, **kwargs):
        resp = super(InvoicePDFEmbededView, self).get(request, *args, **kwargs)
        try:
            if 'send' in self.request.POST:
                self.object.send_email(self.request.POST.get('email_body'))
                messages.add_message(request, messages.SUCCESS, 'Great! The email has been sent out.')
        except:
            log.exception('Unable to send email')
            messages.add_message(request, messages.ERROR, 'Sorry, we were not able to send the email, please try again later.')
        return resp


class InvoicePDFView(TokenSecurityMixin, NormalSecurityMixin, PDFTemplateView):
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'

    def get_context_data(self, **kwargs):
        # temp monkey patch to swap in getters
        ctx = super(InvoicePDFView, self).get_context_data(**kwargs)
        self.object = get_object_or_404(Invoice, uuid=self.kwargs.get('uuid'))
        ctx['object'] = self.object
        self.footer_template = self.new_footer_template
        self.header_template = self.new_header_template
        return ctx

    def get_filename(self):
        return self.request.GET.get('filename', None)

    def get_template_names(self):
        return ['{}/body.html'.format(self.object.business.invoice_theme.path)]

    @property
    def new_header_template(self):
        return '{}/header.html'.format(self.object.business.invoice_theme.path)

    @property
    def new_footer_template(self):
        return '{}/footer.html'.format(self.object.business.invoice_theme.path)

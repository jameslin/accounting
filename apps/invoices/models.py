# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from __future__ import unicode_literals

from datetime import timedelta

from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.utils.timezone import now
from django.contrib.sites.models import Site
from django.core.mail import EmailMultiAlternatives
from apps.contribs.models import BaseModel
from apps.contribs.security.token import get_user_token
from decimal import Decimal
from .managers import InvoiceQuerySet
import requests


class Invoice(BaseModel):
    number = models.IntegerField(blank=True)
    business = models.ForeignKey('businesses.Business', related_name="invoices")
    customer = models.ForeignKey('customers.Customer', related_name="invoices")
    due_date = models.DateField(blank=True)
    paid_at = models.DateField(null=True, blank=True)
    invalid = models.BooleanField(default=False, blank=True)
    objects = InvoiceQuerySet.as_manager()

    def __unicode__(self):
        return u'{} {} -> {}'.format(self.number, self.business, self.customer)

    def get_absolute_url(self):
        return reverse('invoices:update', args=[self.uuid])

    def get_absolute_pdf_url(self, use_token=False):
        if use_token:
            return '{}/pdf?token={}&user={}'.format(reverse('invoices:update', args=[self.uuid]), get_user_token(self.business.user), self.business.user.id)
        return '{}/pdf'.format(reverse('invoices:update', args=[self.uuid]))

    def send_email(self, body=None, to=None, title=None):
        body = body or ''
        to = to or [self.customer.email]
        title = title or 'You have received an invoice from {}'.format(self.business)
        site = Site.objects.get_current()
        mail = EmailMultiAlternatives(
            subject=title,
            body=body,
            from_email='{} <{}>'.format(self.business, self.business.email),
            to=to
        )

        # attach pdf file
        pdf = requests.get('{}://{}{}'.format(settings.HTTP_SCHEME, site.domain, self.get_absolute_pdf_url(use_token=True)), verify=False, stream=True).raw.read()
        mail.attachments = [('{}.pdf'.format(self.number), pdf, 'application/pdf')]
        mail.attach_alternative(body, "text/html")
        mail.send()

    @property
    def total(self):
        return sum(lineitem.subtotal for lineitem in self.line_items.all())

    @property
    def tax_amount(self):
        return sum(lineitem.tax_amount for lineitem in self.line_items.all())

    @property
    def total_include_tax(self):
        return self.total + self.tax_amount

    @property
    def is_overdue(self):
        return not self.invalid and self.due_date < now().date() and self.paid_at is None

    def save(self, *args, **kwargs):
        if not self.number:
            last_number = Invoice.objects.filter(business=self.business).aggregate(models.Max('number'))['number__max']
            if last_number:
                self.number = last_number + 1
            else:
                self.number = self.business.initial_invoice_number

        if not self.due_date:
            self.due_date = timezone.now() + timedelta(days=30)
        return super(Invoice, self).save(*args, **kwargs)


    class Meta:
        ordering = ['-number']
        unique_together = ['number', 'business']


class LineItem(models.Model):
    invoice = models.ForeignKey('Invoice', related_name='line_items')
    particular = models.CharField(max_length=255)
    unit_price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.DecimalField(max_digits=6, decimal_places=2)
    tax = models.DecimalField(max_digits=5, decimal_places=4, null=True, blank=True)

    @property
    def subtotal(self):
        return (self.unit_price * self.quantity).quantize(Decimal('0.00'))

    @property
    def tax_amount(self):
	tax = self.invoice.business.default_tax
	if self.tax is not None:
		tax = self.tax	
        return (self.subtotal * tax).quantize(Decimal('0.00'))

    @property
    def subtotal_include_tax(self):
        return self.subtotal + self.tax_amount


class Theme(BaseModel):
    default = models.BooleanField(default=False)
    published = models.BooleanField(default=False)
    name = models.CharField(max_length=255)
    path = models.CharField(max_length=255, default='invoices/pdf/default/')

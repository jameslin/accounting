# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.conf.urls import url
from . import views
from apps.contribs.regex.url import UUID4_REGEX_PARAM


urlpatterns = [
    url(r'^{}$'.format(UUID4_REGEX_PARAM), views.UpdateInvoiceView.as_view(), name="update"),
    url(r'^{}/pdf$'.format(UUID4_REGEX_PARAM), views.InvoicePDFView.as_view(), name="pdf"),
    url(r'^{}/embed-pdf$'.format(UUID4_REGEX_PARAM), views.InvoicePDFEmbededView.as_view(), name="embed_pdf"),
    url(r'^create$', views.CreateInvoiceView.as_view(), name="create"),
    url(r'^overdue/(?P<page>\d+)$', views.ListOverdueInvoiceView.as_view(), name="overdue"),
    url(r'^overdue$', views.ListOverdueInvoiceView.as_view(), name="overdue"),
    url(r'^(?P<page>\d+)$', views.ListInvoiceView.as_view(), name="list"),
    url(r'^$', views.ListInvoiceView.as_view(), name="list"),
]

# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.db.models import QuerySet, Q
from django.utils.timezone import now


class InvoiceQuerySet(QuerySet):
    def valid(self):
        return self.filter(invalid=False)

    def overdue(self):
        return self.valid().filter(paid_at__isnull=True, due_date__lt=now())

    def normal(self):
        """
        not overdue
        """
        return self.filter(Q(paid_at__isnull=False) | Q(due_date__gt=now()))

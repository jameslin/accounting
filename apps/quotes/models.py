# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from __future__ import unicode_literals

from datetime import timedelta

from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.contrib.sites.models import Site
from django.core.mail import EmailMultiAlternatives
from apps.contribs.models import BaseModel
from apps.contribs.security.token import get_user_token
from decimal import Decimal
from apps.invoices.models import Invoice, LineItem as InvoiceLineItem
from .managers import QuoteQuerySet
import requests


class Quote(BaseModel):
    number = models.IntegerField(blank=True)
    business = models.ForeignKey('businesses.Business', related_name="quotes")
    customer = models.ForeignKey('customers.Customer', related_name="quotes")
    expire_date = models.DateField(blank=True)
    invalid = models.BooleanField(default=False, blank=True)
    objects = QuoteQuerySet.as_manager()

    def __unicode__(self):
        return u'{} {} -> {}'.format(self.number, self.business, self.customer)

    def get_absolute_url(self):
        return reverse('quotes:update', args=[self.uuid])

    def get_absolute_pdf_url(self, use_token=False):
        if use_token:
            return '{}/pdf?token={}&user={}'.format(reverse('quotes:update', args=[self.uuid]), get_user_token(self.business.user), self.business.user.id)
        return '{}/pdf'.format(reverse('quotes:update', args=[self.uuid]))

    def send_email(self, body=None, to=None, title=None):
        body = body or ''
        to = to or [self.customer.email]
        title = title or 'You have received a quote from {}'.format(self.business)
        site = Site.objects.get_current()
        mail = EmailMultiAlternatives(
            subject=title,
            body=body,
            from_email='{} <{}>'.format(self.business, self.business.email),
            to=to
        )

        # attach pdf file
        pdf = requests.get('{}://{}{}'.format(settings.HTTP_SCHEME, site.domain, self.get_absolute_pdf_url(use_token=True)), verify=False, stream=True).raw.read()
        mail.attachments = [('{}.pdf'.format(self.number), pdf, 'application/pdf')]
        mail.attach_alternative(body, "text/html")
        mail.send()

    @property
    def total(self):
        return sum(lineitem.subtotal for lineitem in self.line_items.all())

    @property
    def tax_amount(self):
        return sum(lineitem.tax_amount for lineitem in self.line_items.all())

    @property
    def total_include_tax(self):
        return self.total + self.tax_amount

    def save(self, *args, **kwargs):
        if not self.number:
            last_number = Quote.objects.filter(business=self.business).aggregate(models.Max('number'))['number__max']
            if last_number:
                self.number = last_number + 1
            else:
                self.number = self.business.initial_quote_number

        if not self.expire_date:
            self.expire_date = timezone.now() + timedelta(days=self.business.default_quote_expiry_days)

        return super(Quote, self).save(*args, **kwargs)

    def convert_to_invoice(self):
        invoice = Invoice.objects.create(
            business=self.business,
            customer=self.customer,
        )
        for line_item in self.line_items.all():
            InvoiceLineItem.objects.create(
                invoice=invoice,
                particular=line_item.particular,
                unit_price=line_item.unit_price,
                quantity=line_item.quantity,
                tax=line_item.tax,
            )
        return invoice

    class Meta:
        ordering = ['-number']
        unique_together = ['number', 'business']


class LineItem(models.Model):
    quote = models.ForeignKey('Quote', related_name='line_items')
    particular = models.CharField(max_length=255)
    unit_price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.DecimalField(max_digits=6, decimal_places=2)
    tax = models.DecimalField(max_digits=5, decimal_places=4, null=True, blank=True)

    @property
    def subtotal(self):
        return (self.unit_price * self.quantity).quantize(Decimal('0.00'))

    @property
    def tax_amount(self):
        return (self.subtotal * (self.tax or self.quote.business.default_tax)).quantize(Decimal('0.00'))

    @property
    def subtotal_include_tax(self):
        return self.subtotal + self.tax_amount


class Theme(BaseModel):
    default = models.BooleanField(default=False)
    published = models.BooleanField(default=False)
    name = models.CharField(max_length=255)
    path = models.CharField(max_length=255, default='quotes/pdf/default/')

# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.conf.urls import url
from . import views
from apps.contribs.regex.url import UUID4_REGEX_PARAM


urlpatterns = [
    url(r'^{}$'.format(UUID4_REGEX_PARAM), views.UpdateQuoteView.as_view(), name="update"),
    url(r'^{}/convert-to-invoice$'.format(UUID4_REGEX_PARAM), views.ConvertToInvoiceView.as_view(), name="convert_to_invoice"),
    url(r'^{}/pdf$'.format(UUID4_REGEX_PARAM), views.QuotePDFView.as_view(), name="pdf"),
    url(r'^{}/embed-pdf$'.format(UUID4_REGEX_PARAM), views.QuotePDFEmbededView.as_view(), name="embed_pdf"),
    url(r'^create$', views.CreateQuoteView.as_view(), name="create"),
    url(r'^(?P<page>\d+)$', views.ListQuoteView.as_view(), name="list"),
    url(r'^$', views.ListQuoteView.as_view(), name="list"),
]

from django.contrib import admin
from .models import Quote, Theme


admin.site.register(Quote)
admin.site.register(Theme)

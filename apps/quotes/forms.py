# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from .models import Quote, LineItem
from django import forms as django_forms
from apps.businesses.models import Business
from apps.customers.models import Customer
from apps.contribs.forms.widgets import PercentageInput
from apps.contribs.forms.fields import PercentageField
import floppyforms.__future__ as forms


class QuoteCreateForm(forms.ModelForm):
    class Meta:
        model = Quote
        exclude = ['uuid', 'user', 'paid_at', 'invalid']

    def __init__(self, request, *args, **kwargs):
        super(QuoteCreateForm, self).__init__(*args, **kwargs)
        self.fields['business'].queryset = Business.objects.filter(user=request.user)
        self.fields['customer'].queryset = Customer.objects.filter(user=request.user)
        self.fields['number'].widget = forms.widgets.NumberInput(attrs={'placeholder': 'Empty for auto number'})


class QuoteUpdateForm(QuoteCreateForm):
    class Meta:
        model = Quote
        exclude = ['uuid', 'user']


class LineItemForm(django_forms.ModelForm):
    tax = PercentageField(required=False, widget=PercentageInput(attrs={'placeholder':'Empty for default tax', 'step': 'any'}))

    class Meta:
        model = LineItem
        fields = ['particular', 'unit_price', 'quantity', 'tax']

    def __init__(self, *args, **kwargs):
        super(LineItemForm, self).__init__(*args, **kwargs)
        self.fields['particular'].widget = forms.widgets.TextInput(attrs={'size': 40})

# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django.db.models import QuerySet, Q
from django.utils.timezone import now


class QuoteQuerySet(QuerySet):
    def valid(self):
        return self.filter(invalid=False)

    def expired(self):
        return self.valid().filter(expire_date__lt=now())

    def normal(self):
        """
        not overdue
        """
        return self.filter(expire_date__gt=now())

# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
import logging

from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.views.generic import CreateView, ListView, DetailView, RedirectView
from django.template import Context
from django.template.loader import get_template
from django.contrib import messages
from apps.contribs.views.mixins import NormalSecurityMixin, AutoUserMixin, OwnerQuerySetMixin, InjectRequestFormMixin, TokenSecurityMixin
from extra_views import UpdateWithInlinesView, InlineFormSet
from wkhtmltopdf.views import PDFTemplateView
from .models import Quote, LineItem
from .forms import LineItemForm, QuoteCreateForm, QuoteUpdateForm

log = logging.getLogger(__name__)


class ListQuoteView(NormalSecurityMixin, OwnerQuerySetMixin, ListView):
    model = Quote
    template_name = 'quotes/list.html'
    user_field_name = 'business__user'

    def get_context_data(self, **kwargs):
        ctx = super(ListQuoteView, self).get_context_data(**kwargs)
        ctx['expired'] = Quote.objects.filter(business__user=self.request.user).expired()
        return ctx


class ListExpiredQuoteView(NormalSecurityMixin, OwnerQuerySetMixin, ListView):
    model = Quote
    template_name = 'quotes/expired.html'
    queryset = Quote.objects.expired()
    user_field_name = 'business__user'


class CreateQuoteView(NormalSecurityMixin, AutoUserMixin, InjectRequestFormMixin, CreateView):
    model = Quote
    form_class = QuoteCreateForm
    template_name = 'quotes/create.html'

    def get_initial(self):
        data = super(CreateQuoteView, self).get_initial()
        # if url contains preselected options, then use them
        # eg. create quote from business, customer list
        if self.request.GET.get('business'):
            data['business'] = self.request.GET.get('business')
        if self.request.GET.get('customer'):
            data['customer'] = self.request.GET.get('customer')
        return data


class LineItemInline(NormalSecurityMixin, InlineFormSet):
    model = LineItem
    form_class = LineItemForm
    extra = 10
    max_num = 20


class UpdateQuoteView(NormalSecurityMixin, InjectRequestFormMixin, UpdateWithInlinesView):
    model = Quote
    form_class = QuoteUpdateForm
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'
    inlines = [LineItemInline]
    template_name = 'quotes/update.html'


class ConvertToInvoiceView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        quote = Quote.objects.get(uuid=self.kwargs['uuid'])
        invoice = quote.convert_to_invoice()
        return reverse('invoices:update', kwargs={'uuid': invoice.uuid})


class QuotePDFEmbededView(NormalSecurityMixin, DetailView):
    template_name = 'quotes/pdf.html'
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'
    model = Quote

    def get_context_data(self, **kwargs):
        ctx = super(QuotePDFEmbededView, self).get_context_data(**kwargs)
        ctx['email_body'] = self.request.POST.get('email_body') or get_template('quotes/email/body.txt').render(Context({'object': self.object}))
        return ctx

    def post(self, request, *args, **kwargs):
        resp = super(QuotePDFEmbededView, self).get(request, *args, **kwargs)
        try:
            if 'send' in self.request.POST:
                self.object.send_email(self.request.POST.get('email_body'))
                messages.add_message(request, messages.SUCCESS, 'Great! The email has been sent out.')
        except:
            log.exception('Unable to send email')
            messages.add_message(request, messages.ERROR, 'Sorry, we were not able to send the email, please try again later.')
        return resp


class QuotePDFView(TokenSecurityMixin, NormalSecurityMixin, PDFTemplateView):
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'

    def get_context_data(self, **kwargs):
        # temp monkey patch to swap in getters
        ctx = super(QuotePDFView, self).get_context_data(**kwargs)
        self.object = get_object_or_404(Quote, uuid=self.kwargs.get('uuid'))
        ctx['object'] = self.object
        self.footer_template = self.new_footer_template
        self.header_template = self.new_header_template
        return ctx

    def get_filename(self):
        return self.request.GET.get('filename', None)

    def get_template_names(self):
        return ['{}/body.html'.format(self.object.business.quote_theme.path)]

    @property
    def new_header_template(self):
        return '{}/header.html'.format(self.object.business.quote_theme.path)

    @property
    def new_footer_template(self):
        return '{}/footer.html'.format(self.object.business.quote_theme.path)

# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from rest_framework import permissions


class IsOwnerPermission(permissions.IsAuthenticated):

    def has_object_permission(self, request, view, obj):
        return self.obj.user == self.request.user

# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from .filter_backends import IsOwnerFilterBackend


class FilterUserMixin(object):
    filter_backends = (IsOwnerFilterBackend,)


class UUIDMixin(object):
    lookup_field = 'uuid'


class AutoOwnerMixin(object):

    def perform_create(self, serializer):
        return serializer.save(user=self.request.user)

# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from rest_framework import serializers
from apps.customers.models import Customer


class CustomerSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(read_only=True, required=False)
    created = serializers.DateTimeField(read_only=True, required=False)
    modified = serializers.DateTimeField(read_only=True, required=False)

    class Meta:
        model = Customer
        exclude = ['id', 'user']

# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from rest_framework import viewsets
from apps.customers.models import Customer
from apps.api.contribs.views import FilterUserMixin, UUIDMixin, AutoOwnerMixin
from apps.api.contribs.permissions import IsOwnerPermission
from .serializers import CustomerSerializer


class CustomerViewSet(AutoOwnerMixin, UUIDMixin, FilterUserMixin, viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    permissions = [IsOwnerPermission]

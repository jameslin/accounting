# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework import serializers
from rest_framework.views import APIView
from rest_framework.response import Response
from apps.invoices.models import Invoice, LineItem
from apps.api.contribs.views import UUIDMixin
from .serializers import InvoiceSerializer, LineItemSerializer
import logging

log = logging.getLogger(__name__)


class IsOwnerPermission(permissions.IsAuthenticated):
    """
    slight variation of the one in api.contribs
    """

    def has_object_permission(self, request, view, obj):
        return self.obj.business.user == self.request.user


class InvoiceViewSet(UUIDMixin, viewsets.ModelViewSet):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer
    permissions = [IsOwnerPermission]

    def get_queryset(self):
        return self.queryset.filter(business__user=self.request.user)

    def validate_fk(self, serializer):
        """
        checks if fk is owned by the user
        """
        try:
            assert serializer.validated_data['business'].user == self.request.user
        except:
            raise serializers.ValidationError('Incorrect business specified.')

        try:
            assert serializer.validated_data['customer'].user == self.request.user
        except:
            raise serializers.ValidationError('Incorrect customer specified.')

        # if len(serializer.validated_data['line_items']) > 20:
        #    raise serializers.ValidationError('Too many line items.')

    def perform_create(self, serializer):
        self.validate_fk(serializer)
        return super(InvoiceViewSet, self).perform_create(serializer)

    def perform_update(self, serializer):
        self.validate_fk(serializer)
        return super(InvoiceViewSet, self).perform_update(serializer)


class SendEmailView(APIView):

    def post(self, request, uuid, format=None):
        invoice = Invoice.objects.get(uuid=uuid, business__user=request.user)
        invoice.send_email()
        return Response(True)


class LineItemViewSet(UUIDMixin, viewsets.ModelViewSet):
    queryset = LineItem.objects.all()
    serializer_class = LineItemSerializer

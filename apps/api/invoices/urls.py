# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from rest_framework import routers
from django.conf.urls import url, include

from apps.contribs.regex.url import UUID4_REGEX_PARAM
from .views import InvoiceViewSet, SendEmailView, LineItemViewSet

router = routers.DefaultRouter()
router.register('invoices', InvoiceViewSet)
router.register('lineitem', LineItemViewSet)

urlpatterns = [
    url(r'^send-email/{}$'.format(UUID4_REGEX_PARAM), SendEmailView.as_view(), name='send_email'),
    url(r'^', include(router.urls)),
]

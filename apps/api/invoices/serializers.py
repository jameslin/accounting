# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from rest_framework import serializers
from apps.invoices.models import Invoice, LineItem
from apps.businesses.models import Business
from apps.customers.models import Customer


class LineItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = LineItem
        exclude = ['id']

    invoice = serializers.SlugRelatedField(slug_field='uuid', queryset=Invoice.objects.all())

class NullableIntegerField(serializers.IntegerField):
    def validate_empty_values(self, data):
        return (True, None)


class InvoiceSerializer(serializers.ModelSerializer):
    number = NullableIntegerField(required=False)
    business = serializers.SlugRelatedField(slug_field='uuid', queryset=Business.objects.all())
    customer = serializers.SlugRelatedField(slug_field='uuid', queryset=Customer.objects.all())
    due_date = serializers.DateTimeField(required=False, allow_null=True)
    line_items = LineItemSerializer(many=True, read_only=True)

    class Meta:
        model = Invoice
        exclude = ['id']

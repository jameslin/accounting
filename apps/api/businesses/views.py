# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from rest_framework import viewsets
from apps.businesses.models import Business
from apps.api.contribs.views import FilterUserMixin, UUIDMixin, AutoOwnerMixin
from apps.api.contribs.permissions import IsOwnerPermission
from .serializers import BusinessSerializer


class BusinessViewSet(AutoOwnerMixin, UUIDMixin, FilterUserMixin, viewsets.ModelViewSet):
    queryset = Business.objects.all()
    serializer_class = BusinessSerializer
    permissions = [IsOwnerPermission]

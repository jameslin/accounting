# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from rest_framework import routers
from django.conf.urls import url, include
from .views import BusinessViewSet

router = routers.DefaultRouter()
router.register(r'businesses', BusinessViewSet)

urlpatterns = [
    url(r'^', include(router.urls))
]

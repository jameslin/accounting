# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
import hashlib


def get_user_token(user):
    return hashlib.sha1('{}{}'.format(user.password, user.last_login)).hexdigest()

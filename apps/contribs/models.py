# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
from django_extensions.db.models import TimeStampedModel
from django.db import models
import uuid


class BaseModel(TimeStampedModel):
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)

    class Meta:
        abstract = True
        ordering = ['-id']

    def __unicode__(self):
        try:
            return self.name
        except:
            super(BaseModel, self).__unicode__()

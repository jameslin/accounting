# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
import floppyforms.__future__ as forms
from decimal import Decimal


class PercentageInput(forms.widgets.NumberInput):
    def render(self, name, value, attrs=None):
        if value:
            try:
                value = Decimal(value)
                value = (value * 100).quantize(Decimal('0.00'))
            except ValueError:
                pass
        return super(PercentageInput, self).render(name, value, attrs)

# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import
import floppyforms.__future__ as forms
from .widgets import PercentageInput
from decimal import Decimal


class PercentageField(forms.FloatField):
    widget = PercentageInput

    def to_python(self, value):
        value = super(PercentageField, self).to_python(value)
        if value:
            return Decimal(value / 100).quantize(Decimal('0.0000'))
        else:
            return value

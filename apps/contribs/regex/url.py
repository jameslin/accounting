# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import

UUID4_REGEX = '[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}'
UUID4_REGEX_PARAM = '(?P<uuid>[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12})'
SLUG_REGEX = '[a-z0-9-]+'

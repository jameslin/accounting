from braces.views import LoginRequiredMixin
from django.views.generic.edit import ModelFormMixin
from django.contrib.auth import get_user_model
from apps.contribs.security.token import get_user_token
from django.contrib.auth import login
from django.conf import settings


class NormalSecurityMixin(LoginRequiredMixin):
    pass


class TokenSecurityMixin(LoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):
        User = get_user_model()
        if self.request.user.is_anonymous() and 'token' in self.request.GET and 'user' in self.request.GET:
            user = User.objects.get(id=self.request.GET['user'])
            if get_user_token(user) == self.request.GET['token']:
                user.backend = settings.AUTHENTICATION_BACKENDS[0]
                login(self.request, user)
        return super(TokenSecurityMixin, self).dispatch(request, *args, **kwargs)


class AutoUserMixin(object):
    def form_valid(self, form):
        self.object = form.save(commit=False)
        if not hasattr(self.object, 'user'):
            self.object.user = self.request.user
        self.object.save()
        return super(ModelFormMixin, self).form_valid(form)


class OwnerQuerySetMixin(object):
    user_field_name = 'user'

    def get_queryset(self):
        qs = super(OwnerQuerySetMixin, self).get_queryset()
        qs = qs.filter(**{self.user_field_name: self.request.user})
        return qs


class InjectRequestFormMixin(object):
    def get_form_kwargs(self):
        kwargs = super(InjectRequestFormMixin, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

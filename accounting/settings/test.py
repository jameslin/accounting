from .base import Base  # noqa
import os


class Test(Base):
    DATABASES = {
        'default': {
            'ENGINE': os.environ.get('DJANGO_DB_ENGINE', 'django.db.backends.mysql'),
            'NAME': os.environ.get('DJANGO_DB_NAME', 'invoice'),
            'USER': os.environ.get('DJANGO_DB_USER', 'root'),
            'PASSWORD': os.environ.get('DJANGO_DB_PASSWORD', ''),
        }
    }

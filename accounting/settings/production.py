from .base import Base
import os
from configurations import values


class Production(Base):
    DEBUG = False

    DATABASES = {
        'default': {
            'ENGINE': os.environ.get('DJANGO_DB_ENGINE', 'django.db.backends.mysql'),
            'NAME': os.environ.get('DJANGO_DB_NAME', 'accounting'),
            'USER': os.environ.get('DJANGO_DB_USER', 'accounting'),
            'PASSWORD': os.environ.get('DJANGO_DB_PASSWORD'),
        }
    }

    ADMINS = [('James Lin', 'james@lin.net.nz')]
    ALLOWED_HOSTS = ['*']
    DEFAULT_FROM_EMAIL = values.Value('invoice Server<noreply@invoice.co.nz>')
    WKHTMLTOPDF_CMD = '/usr/local/bin/wkhtmltopdf'

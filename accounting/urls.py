# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.core.urlresolvers import reverse_lazy
from django.views.generic import RedirectView
admin.autodiscover()


urlpatterns = [
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include('apps.api.urls', namespace='api')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r"^account/", include("account.urls")),
    url(r'^businesses/', include('apps.businesses.urls', namespace="businesses")),
    url(r'^customers/', include('apps.customers.urls', namespace="customers")),
    url(r'^invoices/', include('apps.invoices.urls', namespace="invoices")),
    url(r'^quotes/', include('apps.quotes.urls', namespace="quotes")),
    url(r'^$', RedirectView.as_view(url=reverse_lazy('businesses:list')), name='home'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
